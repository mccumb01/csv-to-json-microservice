import pandas as pd
import json
import sys
import requests


def main():
    """
   Main handles cmdline arguments and parses paths for CSV and output files
    """
    num_args = len(sys.argv)
    csv_path = ''
    json_path = ''

    if num_args == 1:
        print("Please supply the CSV file path and/or JSON file path")
        exit(1)

    if num_args > 1:
        csv_path = sys.argv[1]
        print('Your CSV file path: ', csv_path, '\n')
    if num_args > 2:
        json_path = sys.argv[2]
        print('Your JSON file path: ',json_path, '\n')

    csv_to_json(csv_path, json_path)


def csv_to_json(csv_path, json_path):
    """
    Accepts a file path for a CSV file, and an optional path for the JSON
    output. Opens and parses the CSV file to JSON format.
    :param csv_path - Required path to the completed CSV file
    :param [json_path] - optional output path. If not provided, JSON output
    is saved next to the CSV at the same location.
    """
    events = []
    teams = []

    def is_header(string):
        return 'Unnamed' not in string

    try:
        headers = pd.read_csv(csv_path)
        teams = list(filter(is_header, headers.columns))
    except (FileNotFoundError, IOError, Exception) as e:
        print(e, '\nError opening the file. Please ensure it exists and the '
                 'file path is correct')
        exit(1)

    try:
        data = pd.read_csv(csv_path, header=1, na_filter=False)
    except (FileNotFoundError, IOError, Exception) as e:
        print('\nError opening the file. Please ensure it exists and the file '
              'path is correct\n', e)
        exit(1)

    for _, row in data.iterrows():
        time = row[0]
        eventA = {'team': teams[0],
                  'time': time,
                  'eventName': row[1],
                  'location': row[2],
                  'notes': row[3]
                  }

        eventB = {'team': teams[1],
                  'time': time,
                  'eventName': row[4],
                  'location': row[5],
                  'notes': row[6]
                  }

        eventC = {'team': teams[2],
                  'time': time,
                  'eventName': row[7],
                  'location': row[8],
                  'notes': row[9]
                  }
        events.append(eventA)
        events.append(eventB)
        events.append(eventC)

    if json_path == '':
        json_path = csv_path.replace('.csv', '.json')
    post_to_url(events)
    write_json_to_file(json_path, events)


def write_json_to_file(path, data):
    json_str = json.dumps(data, indent=4)
    if '.json' not in path:
        path += '.json'
    try:
        with open(path, 'w') as file:
            file.write(json_str)
    except IOError as e:
        print('\nError writing the JSON file. Be sure you have write '
              'permission at the specified JSON file path\n', e)

    print('Your JSON file is at: ' + path + '\n')


def post_to_url(data, url='https://localhost:7086/api/Events/-1'):
    """
    Sends an HTTP Post request as a JSON body
    :param data: data for the POST body, formatted to JSON below
    :param url: the target URL the data is sent to
    """
    headers = requests.utils.default_headers()
    headers.update(
        {
            "User-Agent": "Python",
            'accept': '*/*',
            'content-type': 'application/json'
        }
    )
    payload = {'json_payload': json.dumps(data)}
    r = requests.post(url,
                      json=data,
                      headers=headers,
                      verify=False)
    print('Sent post to:', url)
    print(r)


if __name__ == '__main__':
    main()
