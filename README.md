This repo contains a custom CSV-to-JSON conversion microservice for the CS 361 class project at Oregon State University.
It is written in Python 3.7 and uses the pandas library and custom transformations for the conversion. 

`csv_to_json.py` is purpose-built to be called/spawned from a classmate's corresponding .NET Event Planner web app. 
That app provides users with the CSV template to fill out, and consumes the resulting JSON in order to display a schedule of events. 
The current MVP version is limited to only 3 teams and 3 specific event categories.

Example Usage: 

```
# Run the service and pass a file path to the CSV template; the JSON file will be saved at the same location with samefilename.json
->>> python3 csv_to_json.py /path/to/the/Schedulr_template.csv 
Your CSV file path:  /path/to/the/Schedulr_template.csv 

Your JSON file is at: /path/to/the/Schedulr_template.json


# You can specify a different output path ending in a filename (otherwise the output will simply be '.json')
->>> python3 csv_to_json.py /path/to/the/Schedulr_template.csv /another/path/to/myjsonfile
Your CSV file path:  /path/to/the/Schedulr_template.csv 

Your JSON file path:  /another/path/to/myjsonfile 

Your JSON file is at: /another/path/to/myjsonfile.json


# Or simply provide a different name that will be saved at the same location
->>> python3 csv_to_json.py /path/to/the/Schedulr_template.csv myjsonhere
Your CSV file path:  /path/to/the/Schedulr_template.csv 

Your JSON file path:  myjsonhere 

Your JSON file is at: myjsonhere.json

# You must at least provide the CSV path - running without it will result in an error
->>> python3 csv_to_json.py  
Please supply the CSV file path and/or JSON file path

#Example raw CSV Data from the Excel template
,Team A - Logistics,,,Team B - Entertainment,,,Team C - Security,,
Start Time,Team A Event,Team A Location,Team A Notes,Team B Event,Team B Location,Team B Notes,Team C Event,Team C Location,Team C Notes
2021-04-23 13:00:00,Event Kickoff,Main Stage,,Event Kickoff,Main Stage,,Event Kickoff,Main Stage,
2021-04-23 13:15:00,Table Setup,Dining Room,,Some Entertainment,The Lounge,tip your waiter,Check IDs,Main Entrance,21+


#Example JSON output

[
    {
        "team": "Team A - Logistics",
        "time": "2021-04-23 13:00:00",
        "eventName": "Event Kickoff",
        "location": "Main Stage",
        "notes": ""
    },
    {
        "team": "Team B - Entertainment",
        "time": "2021-04-23 13:00:00",
        "eventName": "Event Kickoff",
        "location": "Main Stage",
        "notes": ""
    },
    {
        "team": "Team C - Security",
        "time": "2021-04-23 13:00:00",
        "eventName": "Event Kickoff",
        "location": "Main Stage",
        "notes": ""
    },
    {
        "team": "Team A - Logistics",
        "time": "2021-04-23 13:15:00",
        "eventName": "Table Setup",
        "location": "Dining Room",
        "notes": ""
    },
    {
        "team": "Team B - Entertainment",
        "time": "2021-04-23 13:15:00",
        "eventName": "Some Entertainment",
        "location": "The Lounge",
        "notes": "tip your waiter"
    },
    {
        "team": "Team C - Security",
        "time": "2021-04-23 13:15:00",
        "eventName": "Check IDs",
        "location": "Main Entrance",
        "notes": "21+"
    } 
]

```
